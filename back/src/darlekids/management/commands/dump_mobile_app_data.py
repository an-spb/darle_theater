import json

from django.core.management.base import BaseCommand

from darlekids.serializers import MobileAppAllData


class Command(BaseCommand):
    def handle(self, *args, **options):
        data = MobileAppAllData.get_serialized_data()

        data_str = "const initialData = %s;\r\nexport default initialData;" % json.dumps(data, indent=2)

        with open("initialData.ts", "w") as file:
            print(data_str, file=file)

        print('OK')

