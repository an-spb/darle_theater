from django.contrib import admin

from darlekids import models


class SceneInline(admin.TabularInline):
    model = models.FairytaleScene


class FairytaleAdmin(admin.ModelAdmin):
    list_display = ('title', 'order',)
    inlines = (SceneInline,)


class FairytaleKitAdmin(admin.ModelAdmin):
    list_display = ('title_line_1', 'title_line_2', 'ozon_url', 'order')


admin.site.register(models.Fairytale, FairytaleAdmin)
admin.site.register(models.FairytaleKit, FairytaleKitAdmin)
