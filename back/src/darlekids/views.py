import json

from django.views import View
from django.http import HttpResponse

from darlekids.serializers import MobileAppAllData


class FairytalesData(View):
    @staticmethod
    def get(request, *args, **kwargs):
        data = MobileAppAllData.get_serialized_data()
        return HttpResponse(json.dumps(data))
