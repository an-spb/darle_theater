from darlekids.models import Fairytale, FairytaleKit


class MobileAppAllData:
    @staticmethod
    def get_serialized_data():
        return {
            'fairytales': [FairytaleSerializer.serialize(fairytale) for fairytale in Fairytale.objects.all()],
            'kits': [FairytaleKitSerializer.serialize(kit) for kit in FairytaleKit.objects.all()]
        }


class FairytaleSerializer:
    @staticmethod
    def serialize(fairytale):
        return {'id': str(fairytale.id),
                'iconXml': fairytale.icon.read().decode(),
                'title': fairytale.title,
                'characters': fairytale.characters,
                'charactersFull': fairytale.characters_full or None,
                'intro': fairytale.intro or None,
                'scenes': [{'characters': scene.characters or None,
                            'text': scene.text} for scene in fairytale.scenes.all()]}


class FairytaleKitSerializer:
    @staticmethod
    def serialize(kit):
        return {'id': str(kit.id),
                'iconXml': kit.icon.read().decode(),
                'titleLine1': kit.title_line_1,
                'titleLine2': kit.title_line_2,
                'descriptionLine1': kit.description_line_1,
                'descriptionLine2': kit.description_line_2,
                'actors': kit.actors,
                'decorations': kit.decorations,
                'ozonUrl': kit.ozon_url,
                'fairytalesIds': [str(fairytale.id) for fairytale in kit.fairytales.all()]}


