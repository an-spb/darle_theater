# Generated by Django 3.0.6 on 2021-11-03 13:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('darlekids', '0005_auto_20210923_1803'),
    ]

    operations = [
        migrations.AddField(
            model_name='fairytale',
            name='text_characters',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='fairytale',
            name='text_intro',
            field=models.TextField(null=True),
        ),
        migrations.CreateModel(
            name='FairytaleScene',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text_characters', models.TextField(null=True)),
                ('text', models.TextField(null=True)),
                ('order', models.IntegerField(default=1000)),
                ('fairytale', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='scenes', to='project.Fairytale')),
            ],
            options={
                'ordering': ('order',),
            },
        ),
    ]
