from django.db import models


class Fairytale(models.Model):
    icon = models.FileField()
    title = models.CharField(max_length=100)
    characters = models.CharField(max_length=255, null=True)
    characters_full = models.TextField(null=True)
    intro = models.TextField(null=True, blank=True)
    order = models.IntegerField(default=1000)

    __str__ = lambda self: self.title

    class Meta:
        ordering = ('order',)
        db_table = 'project_fairytale'


class FairytaleScene(models.Model):
    fairytale = models.ForeignKey(Fairytale, on_delete=models.CASCADE, related_name='scenes')
    characters = models.TextField(null=True)
    text = models.TextField(null=True)
    order = models.IntegerField(default=1000)

    class Meta:
        ordering = ('order',)


class FairytaleKit(models.Model):
    fairytales = models.ManyToManyField(Fairytale, blank=True)
    icon = models.FileField(null=True)
    title_line_1 = models.CharField(max_length=255, null=True)
    title_line_2 = models.CharField(max_length=255, null=True, blank=True)
    description_line_1 = models.CharField(max_length=255, null=True)
    description_line_2 = models.CharField(max_length=255, null=True)
    actors = models.CharField(max_length=255, null=True)
    decorations = models.CharField(max_length=255, null=True)
    ozon_url = models.CharField(max_length=255)
    order = models.IntegerField(default=1000)

    __str__ = lambda self: self.title_line_1

    class Meta:
        ordering = ('order',)
        db_table = 'project_fairytalekit'


