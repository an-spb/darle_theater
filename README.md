## DarleKids

DarleKids is an assistant application of a storyteller
for the kids' shadow theater kit.

Search this app in AppStore and GooglePlay by "DarleKids".

### /front folder

Basic react-native project with redux structure. 

The code is under [/front/app](front/app) folder.

### /back folder

Small content management system on Django.

The code is under [/back/src](back/src) folder.







